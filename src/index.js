import React from 'react'
import ReactDom from 'react-dom'
import App from './App/App'

// const el = React.createElement (
// 	'h1',
// 	null,
// 	"Hello world"
// )

// ReactDom.render(el,document.getElementById("root"))

// jsx современная версия


// React component

// arrow function

// const List = () => (
// 	<ul>
// 	<li>List item</li>
// 	<li>List item</li>
// </ul>
// )

// const List = () => {
// 	return (
// 	<ul>
// 	<li>List item</li>
// 	<li>List item</li>
// </ul>
// )
// }

// function declaration

// function List( ) {
// 	return (
// 		<ul>
// 		<li>List item</li>
// 		<li>List item</li>
// 	</ul>
// 	)
// }

// const Header = () => {
// 	return (
// 	<h1>My work</h1>
// )
// }

// // React Element
// const el = 
// <div>
// 	<h1>Hello world!!!</h1>
// 	<p>Lorem ipsum bla bla</p>
// 	<List/>
// 	<Header/>
// </div>


// ReactDom.render(el,document.getElementById("root"))

ReactDom.render(
	<App/>,document.getElementById("root"))