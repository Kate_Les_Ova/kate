import React from 'react'
import PropTypes from 'prop-types';
import "./ProductListItem.css"

// Дестректуризация

const ProductListItem = ({
    name,
    description = "No description...", 
    // (второй способ дефолтного значения)
    type,
    capacity,
    price,
    image = "/images/unnamed.png"
}) => {
    return (
        <div className="product-list-item">
                <div className="product-img">
                    <img src={image}alt=""/>
                </div>
                <div className="product-title">{name}</div>
                <div className="product-description">{description}</div>
                <div className="product-features">Type: {type}</div>
                <div className="product-features">Capacity:{capacity}</div>
                <div className="product-price">${price}</div>
                <button className="btn-add-to-cart">Add to csrt</button>
            </div>
    )
}

// Через пропсы

// const ProductListItem = (props) => {
//     return (
//         <div className="product-list-item">
//                 <div className="product-title">{props.name}</div>
//                 <div className="product-description">{props.description}</div>
//                 <div className="product-features">Type: {props.type}</div>
//                 <div className="product-features">Capacity:{props.capacity}</div>
//                 <div className="product-price">${props.price}</div>
//                 <button className="btn-add-to-cart">Add to csrt</button>
//             </div>
//     )
// }

ProductListItem.propTypes = {
    name:PropTypes.string.isRequired,
    description:PropTypes.string,
    type:PropTypes.string.isRequired,
    capacity:PropTypes.number.isRequired,
    price:PropTypes.number.isRequired,
    // image:PropTypes.object.isRequired,
}

// Первый способ задать дефолтное значение


// ProductListItem.defaultProps = {
//     description:"No description..."
// }

export default ProductListItem