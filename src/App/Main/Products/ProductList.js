import React from 'react'
import producstData from './productsData'
import ProductListItem from "./ProductListItem"


const ProductList = () => {
    return (
        <div className="product-list">
            <h1 className="page-title">Product List</h1>
                <div className="row">

                    {/* <div className="col-lg-6">
                        <ProductListItem
                        name="iPhone X"
                        description="This is iPhone X"
                        type="phone"
                        capacity="64"
                        price="500"
                        />
                    </div>
                    <div className="col-lg-6">
                        <ProductListItem
                         name="iPhone XS"
                         description="This is iPhone XS"
                         type="phone"
                         capacity="34"
                         price="1000"
                         />
                    </div>
                    <div className="col-lg-6">
                        <ProductListItem
                         name="iPhone 8 plus"
                         description="This is iPhone 8 plus"
                         type="phone"
                         capacity="128"
                         price="1500"
                        />
                    </div>
                    <div className="col-lg-6">
                        <ProductListItem
                         name="iPhone 8"
                         description="This is iPhone 8"
                         type="phone"
                         capacity="64"
                         price="2500"
                        />
                    </div> */}

{/* // Дестректуризация   */}

                {
                    producstData.map(({
                            id,
                           name,
                           description,
                           type,
                           capacity,
                           price,
                           image
                       }) => (
                           <div className="col-lg-6" key={id}>
                        <ProductListItem
                         name={name}
                         description={description}
                         type={type}
                         capacity={capacity}
                         price={price}
                         image={image}
                         />
                    </div>
                    ))
                }

{/* // Через пропсы */}

{/* {
                    producstData.map((product) => (
                        <div className="col-lg-6" key={product.id}>
                        <ProductListItem
                         name={product.name}
                         description={product.description}
                         type={product.type}
                         capacity={product.capacity}
                         price={product.price}
                         />
                    </div>
                    ))
                } */}
                </div>
            </div>
    )
}


export default ProductList